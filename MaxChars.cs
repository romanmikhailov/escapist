﻿namespace Escapist
{
    public enum MaxChars
    {
        Pike = 80,
        Elite = 96,
        PikeCondensed = 137,
        EliteCondensed = 160,
    }
}