﻿namespace Escapist
{
    public enum TextPosition
    {
        Normal,
        Subscript,
        Superscript,
    }
}
