﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Escapist
{
    public class MainForm : Form
    {
        public Dictionary<byte, CmdAction> Actions = new Dictionary<byte, CmdAction>();
        public InteractiveConsoleForm InteractiveConsole = new InteractiveConsoleForm();
        private IContainer components;
        private OpenFileDialog binOpenDialog;
        private StatusStrip statusStrip1;
        private Panel picturePanel;
        private PictureBox OutputPictureBox;
        private MenuStrip menuStrip;
        private ToolStripMenuItem fileMenuGroup;
        private ToolStripMenuItem exitMenuItem;
        private ToolStripMenuItem helpMenuGroup;
        private ToolStripMenuItem openMenuItem;
        private ToolStripMenuItem exportMenuGroup;
        private ToolStripMenuItem exportPngMenuItem;
        private SaveFileDialog pngSaveDialog;
        private ToolStripMenuItem emulationMenuGroup;
        private ToolStripMenuItem clearMenuItem;
        private ToolStripMenuItem encodingMenuGroup;
        private ToolStripMenuItem cP1251ToolStripMenuItem;
        private ToolStripMenuItem cP866ToolStripMenuItem;
        private ToolStripMenuItem AboutMenuItem;
        private ToolStripMenuItem offsetCalcMenuItem;
        private ToolStripMenuItem interactiveConsoleMenuItem;
        public Printer Printer;
        public Bitmap Canvas;

        public MainForm()
        {
            this.InitializeComponent();
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.Canvas = new Bitmap(this.OutputPictureBox.Width, this.OutputPictureBox.Height);
            this.Printer = new Printer(Graphics.FromImage((Image)this.Canvas), this.Actions);
            this.FillActionList();
            this.InteractiveConsole.Printer = this.Printer;
            this.InteractiveConsole.MainFormPictureBox = this.OutputPictureBox;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.binOpenDialog = new OpenFileDialog();
            this.statusStrip1 = new StatusStrip();
            this.picturePanel = new Panel();
            this.OutputPictureBox = new PictureBox();
            this.menuStrip = new MenuStrip();
            this.fileMenuGroup = new ToolStripMenuItem();
            this.openMenuItem = new ToolStripMenuItem();
            this.exportMenuGroup = new ToolStripMenuItem();
            this.exportPngMenuItem = new ToolStripMenuItem();
            this.exitMenuItem = new ToolStripMenuItem();
            this.emulationMenuGroup = new ToolStripMenuItem();
            this.clearMenuItem = new ToolStripMenuItem();
            this.interactiveConsoleMenuItem = new ToolStripMenuItem();
            this.encodingMenuGroup = new ToolStripMenuItem();
            this.cP1251ToolStripMenuItem = new ToolStripMenuItem();
            this.cP866ToolStripMenuItem = new ToolStripMenuItem();
            this.offsetCalcMenuItem = new ToolStripMenuItem();
            this.helpMenuGroup = new ToolStripMenuItem();
            this.AboutMenuItem = new ToolStripMenuItem();
            this.pngSaveDialog = new SaveFileDialog();
            this.picturePanel.SuspendLayout();
            ((ISupportInitialize)this.OutputPictureBox).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            this.binOpenDialog.Filter = "\"Файлы *.bin|*.bin|All files|*.*\"";
            this.binOpenDialog.FileOk += new CancelEventHandler(this.binOpenDialog_FileOk);
            this.binOpenDialog.RestoreDirectory = true;
            this.statusStrip1.Location = new Point(0, 633);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new Size(814, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            this.picturePanel.AutoScroll = true;
            this.picturePanel.Controls.Add((Control)this.OutputPictureBox);
            this.picturePanel.Location = new Point(0, 27);
            this.picturePanel.Name = "picturePanel";
            this.picturePanel.Size = new Size(812, 603);
            this.picturePanel.TabIndex = 6;
            this.OutputPictureBox.BackColor = SystemColors.Control;
            this.OutputPictureBox.Location = new Point(0, 0);
            this.OutputPictureBox.Name = "OutputPictureBox";
            this.OutputPictureBox.Size = new Size(792, 1015);
            this.OutputPictureBox.TabIndex = 1;
            this.OutputPictureBox.TabStop = false;
            this.OutputPictureBox.Paint += new PaintEventHandler(this.OutputPictureBox_Paint);
            this.menuStrip.Items.AddRange(new ToolStripItem[3]
            {
        (ToolStripItem) this.fileMenuGroup,
        (ToolStripItem) this.emulationMenuGroup,
        (ToolStripItem) this.helpMenuGroup
            });
            this.menuStrip.Location = new Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new Size(814, 24);
            this.menuStrip.TabIndex = 7;
            this.menuStrip.Text = "menuStrip1";
            this.fileMenuGroup.DropDownItems.AddRange(new ToolStripItem[3]
            {
        (ToolStripItem) this.openMenuItem,
        (ToolStripItem) this.exportMenuGroup,
        (ToolStripItem) this.exitMenuItem
            });
            this.fileMenuGroup.Name = "fileMenuGroup";
            this.fileMenuGroup.Size = new Size(48, 20);
            this.fileMenuGroup.Text = "Файл";
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.Size = new Size(152, 22);
            this.openMenuItem.Text = "Открыть...";
            this.openMenuItem.Click += new EventHandler(this.openMenuItem_Click);
            this.exportMenuGroup.DropDownItems.AddRange(new ToolStripItem[1]
            {
        (ToolStripItem) this.exportPngMenuItem
            });
            this.exportMenuGroup.Name = "exportMenuGroup";
            this.exportMenuGroup.Size = new Size(152, 22);
            this.exportMenuGroup.Text = "Экспорт";
            this.exportPngMenuItem.Name = "exportPngMenuItem";
            this.exportPngMenuItem.Size = new Size(98, 22);
            this.exportPngMenuItem.Text = "PNG";
            this.exportPngMenuItem.Click += new EventHandler(this.exportPngMenuItem_Click);
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new Size(152, 22);
            this.exitMenuItem.Text = "Выход";
            this.exitMenuItem.Click += new EventHandler(this.exitMenuItem_Click);
            this.emulationMenuGroup.DropDownItems.AddRange(new ToolStripItem[4]
            {
        (ToolStripItem) this.clearMenuItem,
        (ToolStripItem) this.interactiveConsoleMenuItem,
        (ToolStripItem) this.encodingMenuGroup,
        (ToolStripItem) this.offsetCalcMenuItem
            });
            this.emulationMenuGroup.Name = "emulationMenuGroup";
            this.emulationMenuGroup.Size = new Size(74, 20);
            this.emulationMenuGroup.Text = "Эмуляция";
            this.clearMenuItem.Name = "clearMenuItem";
            this.clearMenuItem.Size = new Size(202, 22);
            this.clearMenuItem.Text = "Новый лист";
            this.clearMenuItem.Click += new EventHandler(this.clearMenuItem_Click);
            this.interactiveConsoleMenuItem.Name = "interactiveConsoleMenuItem";
            this.interactiveConsoleMenuItem.Size = new Size(202, 22);
            this.interactiveConsoleMenuItem.Text = "Интерактивный режим";
            this.interactiveConsoleMenuItem.Click += new EventHandler(this.interactiveConsoleMenuItem_Click);
            this.encodingMenuGroup.DropDownItems.AddRange(new ToolStripItem[2]
            {
        (ToolStripItem) this.cP1251ToolStripMenuItem,
        (ToolStripItem) this.cP866ToolStripMenuItem
            });
            this.encodingMenuGroup.Name = "encodingMenuGroup";
            this.encodingMenuGroup.Size = new Size(202, 22);
            this.encodingMenuGroup.Text = "Кодировка";
            this.cP1251ToolStripMenuItem.Checked = true;
            this.cP1251ToolStripMenuItem.CheckOnClick = true;
            this.cP1251ToolStripMenuItem.CheckState = CheckState.Checked;
            this.cP1251ToolStripMenuItem.Name = "cP1251ToolStripMenuItem";
            this.cP1251ToolStripMenuItem.Size = new Size(116, 22);
            this.cP1251ToolStripMenuItem.Text = "CP 1251";
            this.cP1251ToolStripMenuItem.Click += new EventHandler(this.cP1251ToolStripMenuItem_Click);
            this.cP866ToolStripMenuItem.CheckOnClick = true;
            this.cP866ToolStripMenuItem.Name = "cP866ToolStripMenuItem";
            this.cP866ToolStripMenuItem.Size = new Size(116, 22);
            this.cP866ToolStripMenuItem.Text = "CP 866";
            this.cP866ToolStripMenuItem.Click += new EventHandler(this.cP866ToolStripMenuItem_Click);
            this.offsetCalcMenuItem.Name = "offsetCalcMenuItem";
            this.offsetCalcMenuItem.Size = new Size(202, 22);
            this.offsetCalcMenuItem.Text = "Калькулятор отступов";
            this.offsetCalcMenuItem.Click += new EventHandler(this.offsetCalcMenuItem_Click);
            this.helpMenuGroup.DropDownItems.AddRange(new ToolStripItem[1]
            {
        (ToolStripItem) this.AboutMenuItem
            });
            this.helpMenuGroup.Name = "helpMenuGroup";
            this.helpMenuGroup.Size = new Size(65, 20);
            this.helpMenuGroup.Text = "Справка";
            this.AboutMenuItem.Name = "AboutMenuItem";
            this.AboutMenuItem.Size = new Size(149, 22);
            this.AboutMenuItem.Text = "О программе";
            this.AboutMenuItem.Click += new EventHandler(this.AboutMenuItem_Click);
            this.pngSaveDialog.Filter = "\"Файлы *.png|*.png\"";
            this.pngSaveDialog.FileOk += new CancelEventHandler(this.pngSaveDialog_FileOk);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(814, 655);
            this.Controls.Add((Control)this.picturePanel);
            this.Controls.Add((Control)this.statusStrip1);
            this.Controls.Add((Control)this.menuStrip);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Escapist";
            this.picturePanel.ResumeLayout(false);
            ((ISupportInitialize)this.OutputPictureBox).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private void FillActionList()
        {
            this.Actions.Add(Escp.Commands["Reset"].CmdByte, new CmdAction(this.Printer.Reset));
            this.Actions.Add((byte)13, new CmdAction(this.Printer.NewLine));
            this.Actions.Add(Escp.Commands["NewLineN216"].CmdByte, new CmdAction(this.Printer.NewLineN216));
            this.Actions.Add(Escp.Commands["LeftMargin"].CmdByte, new CmdAction(this.Printer.SetLeftMargin));
            this.Actions.Add(Escp.Commands["SetPike"].CmdByte, new CmdAction(this.Printer.SetPike));
            this.Actions.Add(Escp.Commands["SetElite"].CmdByte, new CmdAction(this.Printer.SetElite));
            this.Actions.Add(Escp.Commands["SetCondensed"].CmdByte, new CmdAction(this.Printer.SetCondensed));
            this.Actions.Add(Escp.Commands["SetAccentBold"].CmdByte, new CmdAction(this.Printer.SetAccentBold));
            this.Actions.Add(Escp.Commands["UnsetAccentBold"].CmdByte, new CmdAction(this.Printer.UnsetAccentBold));
            this.Actions.Add(Escp.Commands["SetDoubleStrike"].CmdByte, new CmdAction(this.Printer.SetDoubleStrike));
            this.Actions.Add(Escp.Commands["UnsetDoubleStrike"].CmdByte, new CmdAction(this.Printer.UnsetDoubleStrike));
            this.Actions.Add(Escp.Commands["LQMode"].CmdByte, new CmdAction(this.Printer.ToggleQuality));
            this.Actions.Add(Escp.Commands["SetAbsHorizontalOffset"].CmdByte, new CmdAction(this.Printer.AbsHorizontalOffset));
            this.Actions.Add(Escp.Commands["SetRelHorizontalOffset"].CmdByte, new CmdAction(this.Printer.RelHorizontalOffset));
            this.Actions.Add(Escp.Commands["ToggleDoubleWidth"].CmdByte, new CmdAction(this.Printer.ToggleDoubleWidth));
            this.Actions.Add(Escp.Commands["ToggleDoubleHeight"].CmdByte, new CmdAction(this.Printer.ToggleDoubleHeight));
            this.Actions.Add(Escp.Commands["ToggleUnderline"].CmdByte, new CmdAction(this.Printer.ToggleUnderline));
            this.Actions.Add(Escp.Commands["SetScript"].CmdByte, new CmdAction(this.Printer.SetScript));
            this.Actions.Add(Escp.Commands["ResetScript"].CmdByte, new CmdAction(this.Printer.ResetScript));
            this.Actions.Add(Escp.Commands["SetPxPerLineN72"].CmdByte, new CmdAction(this.Printer.SetPxPerLineN72));
            this.Actions.Add(Escp.Commands["SetPxPerLineN216"].CmdByte, new CmdAction(this.Printer.SetPxPerLineN216));
            this.Actions.Add(Escp.Commands["DefineUserSymbol"].CmdByte, new CmdAction(this.Printer.DefineUserSymbol));
            this.Actions.Add(Escp.Commands["SelectCharacterSet"].CmdByte, new CmdAction(this.Printer.SelectCharacterSet));
            this.Actions.Add(Escp.Commands["SetGraphicsModeSingle"].CmdByte, new CmdAction(this.Printer.SetGraphicsMode));
            this.Actions.Add(Escp.Commands["SetGraphicsModeDouble"].CmdByte, new CmdAction(this.Printer.SetGraphicsMode));
            this.Actions.Add(Escp.Commands["SetGraphicsModeDoubleHiSpeed"].CmdByte, new CmdAction(this.Printer.SetGraphicsMode));
        }

        private void Button1Click(object sender, EventArgs e)
        {
        }

        private void binOpenDialog_FileOk(object sender, CancelEventArgs e)
        {
            this.Printer.PrintFromFile(this.binOpenDialog.FileName);
            this.OutputPictureBox.Refresh();
        }

        private void OutputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage((Image)this.Canvas, 0, 0);
        }

        private void exitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void exportPngMenuItem_Click(object sender, EventArgs e)
        {
            int num = (int)this.pngSaveDialog.ShowDialog();
        }

        private void pngSaveDialog_FileOk(object sender, CancelEventArgs e)
        {
            this.Canvas.Save(this.pngSaveDialog.FileName, ImageFormat.Png);
        }

        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            int num = (int)MessageBox.Show("Escapist 1.0.1 by DaQuirm", "Escapist", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void cP1251ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Printer.Encoding = Encoding.GetEncoding(1251);
            this.cP1251ToolStripMenuItem.CheckState = CheckState.Checked;
            this.cP866ToolStripMenuItem.CheckState = CheckState.Unchecked;
        }

        private void cP866ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Printer.Encoding = Encoding.GetEncoding(866);
            this.cP1251ToolStripMenuItem.CheckState = CheckState.Unchecked;
            this.cP866ToolStripMenuItem.CheckState = CheckState.Checked;
        }

        private void openMenuItem_Click(object sender, EventArgs e)
        {
            int num1 = (int)this.binOpenDialog.ShowDialog();
            if (!this.Printer.ErrorFlag)
                return;
            int num2 = (int)MessageBox.Show("Эмуляция завершилась с ошибкой. \r\nСм. файл log.txt в каталоге программы. \r\nСообщения об ошибках начинаются с символов [!]", "Escapist", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void offsetCalcMenuItem_Click(object sender, EventArgs e)
        {
            int num = (int)new OffsetCalcForm().ShowDialog();
        }

        private void clearMenuItem_Click(object sender, EventArgs e)
        {
            this.Printer.NewPaper();
            this.OutputPictureBox.Refresh();
        }

        private void interactiveConsoleMenuItem_Click(object sender, EventArgs e)
        {
            this.InteractiveConsole = new InteractiveConsoleForm();
            int num = (int)this.InteractiveConsole.ShowDialog();
        }
    }
}
