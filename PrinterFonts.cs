﻿using System.Drawing;

namespace Escapist
{
    public static class PrinterFonts
    {
        public static Font Pike = new Font("Courier", 11.7f);
        public static Font Elite = new Font("Courier", 9.8f);
        public static Font PikeCondensed = new Font("Monofonto", 10.2f);
        public static Font EliteCondensed = new Font("Monofonto", 8.7f);
        public static Font LetterQuality = new Font("Fixedsys", 9f);
    }
}
