﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Escapist
{
    public class InteractiveConsoleForm : Form
    {
        public Printer Printer;
        public PictureBox MainFormPictureBox;
        private IContainer components;
        private Button execButton;
        private TextBox consoleOutput;
        private TextBox cmdLine;
        private Label inputLabel;
        private ComboBox inputMode;

        public InteractiveConsoleForm()
        {
            this.InitializeComponent();
            this.inputMode.SelectedIndex = 0;
        }

        private void execButton_Click(object sender, EventArgs e)
        {
            switch (this.inputMode.SelectedIndex)
            {
                case 0:
                    EscpCommand cmd = new EscpCommand(Encoding.GetEncoding(1251));
                    cmd.ParseFromString(this.cmdLine.Text);
                    this.Printer.ExecuteCommand(cmd);
                    break;
                case 1:
                    this.Printer.PrintText(this.cmdLine.Text);
                    break;
            }
            this.MainFormPictureBox.Refresh();
            this.consoleOutput.AppendText(this.cmdLine.Text + "\r\n");
            this.cmdLine.Clear();
        }

        private void cmdLine_TextChanged(object sender, EventArgs e)
        {
            this.inputMode.SelectedIndex = !Regex.IsMatch(this.cmdLine.Text, "^[\\sA-Fa-f0-9]*$") ? 1 : 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.execButton = new Button();
            this.consoleOutput = new TextBox();
            this.cmdLine = new TextBox();
            this.inputLabel = new Label();
            this.inputMode = new ComboBox();
            this.SuspendLayout();
            this.execButton.Location = new Point(570, 518);
            this.execButton.Name = "execButton";
            this.execButton.Size = new Size(75, 23);
            this.execButton.TabIndex = 0;
            this.execButton.Text = "Выполнить";
            this.execButton.UseVisualStyleBackColor = true;
            this.execButton.Click += new EventHandler(this.execButton_Click);
            this.consoleOutput.Location = new Point(12, 12);
            this.consoleOutput.Multiline = true;
            this.consoleOutput.Name = "consoleOutput";
            this.consoleOutput.ScrollBars = ScrollBars.Vertical;
            this.consoleOutput.Size = new Size(633, 500);
            this.consoleOutput.TabIndex = 1;
            this.cmdLine.Location = new Point(12, 521);
            this.cmdLine.Name = "cmdLine";
            this.cmdLine.Size = new Size(552, 20);
            this.cmdLine.TabIndex = 2;
            this.cmdLine.TextChanged += new EventHandler(this.cmdLine_TextChanged);
            this.inputLabel.AutoSize = true;
            this.inputLabel.Location = new Point(12, 554);
            this.inputLabel.Name = "inputLabel";
            this.inputLabel.Size = new Size(32, 13);
            this.inputLabel.TabIndex = 3;
            this.inputLabel.Text = "Ввод";
            this.inputMode.FormattingEnabled = true;
            this.inputMode.Items.AddRange(new object[2]
            {
        (object) "ESC-P байты (HEX)",
        (object) "Текст"
            });
            this.inputMode.Location = new Point(50, 551);
            this.inputMode.Name = "inputMode";
            this.inputMode.Size = new Size(121, 21);
            this.inputMode.TabIndex = 4;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(657, 576);
            this.Controls.Add((Control)this.inputMode);
            this.Controls.Add((Control)this.inputLabel);
            this.Controls.Add((Control)this.cmdLine);
            this.Controls.Add((Control)this.consoleOutput);
            this.Controls.Add((Control)this.execButton);
            this.Name = "InteractiveConsoleForm";
            this.Text = "Интерактивный режим";
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}
