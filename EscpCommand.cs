﻿using System;
using System.Text;

namespace Escapist
{
    public class EscpCommand
    {
        public byte[] ParamBytes;
        public CommandFormat Format;
        public byte[] SymbolBytes;
        private readonly Encoding Encoding;

        public bool IsEscapeCommand { get; private set; }

        public bool HasText { get; private set; }

        public string Text { get; private set; }

        public EscpCommand(Encoding encoding)
        {
            this.Encoding = encoding;
        }

        public void ParseFromString(string cmdString)
        {
            string[] strArray = cmdString.Split(' ');
            if (strArray[0] == "ESC" || strArray[0] == 27.ToString())
                strArray[0] = "1B";
            byte[] numArray = new byte[strArray.Length];
            int num = 0;
            foreach (string str in strArray)
                numArray[num++] = Convert.ToByte(str, 16);
            this.ParseFromBytes(numArray);
        }

        public void ParseFromBytes(params byte[] values)
        {
            if ((int)values[0] == 27)
            {
                this.Format = Escp.GetFormat(values[1]);
                if (values.Length >= this.Format.Length)
                {
                    if (this.Format.Length > 2)
                    {
                        this.ParamBytes = new byte[this.Format.Length - 2];
                        for (int index = 2; index < this.Format.Length; ++index)
                            this.ParamBytes[index - 2] = values[index];
                    }
                    this.IsEscapeCommand = true;
                    if (values.Length <= this.Format.Length || (int)this.Format.CmdByte == (int)Escp.UnknownCommand.CmdByte)
                        return;
                    if (this.Format.Name == "DefineUserSymbol" || this.Format.Name.StartsWith("SetGraphicsMode"))
                    {
                        this.SymbolBytes = new byte[values.Length - this.Format.Length];
                        Array.Copy((Array)values, this.Format.Length, (Array)this.SymbolBytes, 0, values.Length - this.Format.Length);
                    }
                    else
                    {
                        byte[] bytes = new byte[values.Length - this.Format.Length];
                        Array.Copy((Array)values, this.Format.Length, (Array)bytes, 0, values.Length - this.Format.Length);
                        this.Text = this.Encoding.GetString(bytes);
                        this.HasText = true;
                    }
                }
                else
                    this.Format = Escp.UnknownCommand;
            }
            else
            {
                if ((int)values[0] != 13 || (int)values[1] != 10)
                    return;
                this.IsEscapeCommand = false;
                this.Format = Escp.NewLineCommand;
                this.ParamBytes = new byte[2];
                this.ParamBytes[0] = (byte)13;
                this.ParamBytes[1] = (byte)10;
            }
        }

        public override string ToString()
        {
            string str;
            if (this.IsEscapeCommand)
            {
                str = string.Format("ESC {0:x}", (object)this.Format.CmdByte);
                if (this.Format.Length > 2)
                    str += string.Format(" параметры:{0}", (object)BitConverter.ToString(this.ParamBytes).Replace("-", " "));
            }
            else
                str = "0d 0a";
            return str;
        }
    }
}
