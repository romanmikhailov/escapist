﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Escapist
{
    public class Printer
    {
        public int PxPerLine = 12;
        private readonly Pen blackPen = new Pen(Color.Black);
        private readonly Pen whitePen = new Pen(Color.White);
        public Graphics Output;
        public Font Font;
        public Paper Paper;
        public Encoding Encoding;
        public Margin Margins;
        public Cursor Cursor;
        public TextPosition TextPosition;
        public PrintQuality Quality;
        public float Unit;
        public int CurrentFontMaxChars;
        public int HorizontalOffset;
        public Dictionary<byte, CmdAction> ActionList;
        public Dictionary<byte, UserSymbol> UserSymbols;
        public StreamWriter LogFile;
        public bool IsNormalCharSet = true;
        private bool AccentBold;
        private bool DoubleStrike;
        private bool DoubleWidth;
        private bool DoubleHeight;
        private bool Underline;

        public bool ErrorFlag { get; private set; }

        public Printer(Graphics printerOutput, Dictionary<byte, CmdAction> actionList)
        {
            this.Output = printerOutput;
            this.ActionList = actionList;
            this.Encoding = Encoding.GetEncoding(1251);
            this.Paper = new Paper(778, 1000);
            this.Margins = new Margin()
            {
                Bottom = 4,
                Left = 4,
                Right = 4,
                Top = 4
            };
            this.Cursor = new Cursor()
            {
                Line = 1,
                Column = 0,
                X = 0.0f,
                Y = (float)this.Margins.Top
            };
            this.HorizontalOffset = 0;
            this.Font = PrinterFonts.Pike;
            this.CurrentFontMaxChars = 80;
            this.Quality = PrintQuality.Draft;
            this.Unit = (float)this.Paper.Width / 480f;
            this.Underline = false;
            this.TextPosition = TextPosition.Normal;
            this.Clear();
            this.UserSymbols = new Dictionary<byte, UserSymbol>();
        }

        public void PrintFromFile(string fileName)
        {
            this.Reset();
            this.NewPaper();
            BinParser binParser = new BinParser();
            binParser.ReadFile(fileName);
            this.LogFile = new StreamWriter("log.txt");
            this.ErrorFlag = false;
            foreach (byte[] numArray in binParser.CmdList)
            {
                EscpCommand cmd = new EscpCommand(this.Encoding);
                cmd.ParseFromBytes(numArray);
                this.ExecuteCommand(cmd);
            }
            this.LogFile.Close();
        }

        public void DrawLayout()
        {
            this.Output.DrawRectangle(new Pen(Color.Black), this.Margins.Left, this.Margins.Top, this.Paper.Width - this.Margins.Left - this.Margins.Right, this.Paper.Height - this.Margins.Top - this.Margins.Bottom);
        }

        public void ExecuteCommand(EscpCommand cmd)
        {
            if ((int)cmd.Format.CmdByte != (int)Escp.UnknownCommand.CmdByte)
            {
                if (cmd.Format.Name.StartsWith("SetGraphicsMode"))
                    this.PrintGraphics(cmd);
                else
                {
                    if (ActionList.ContainsKey(cmd.Format.CmdByte))
                    {
                        ActionList[cmd.Format.CmdByte](cmd);
                    }
                    else
                    {
                        if (null != this.LogFile)
                        {
                            LogFile.Write("Unsupported command! ");
                        }
                    }
                }
                    
                if (cmd.Format.Name == "NewLine")
                {
                    if (this.LogFile != null)
                        this.LogFile.WriteLine("---Новая строка---");
                }
                else if (this.LogFile != null)
                    this.LogFile.WriteLine("{0}(ESC {1:x}) длина: {2} HEX dump:{3}", (object)cmd.Format.RuName, (object)cmd.Format.CmdByte, (object)cmd.Format.Length, (object)cmd);
                if (!cmd.HasText)
                    return;
                this.PrintText(cmd.Text);
            }
            else if (cmd.Format.Name == "NewLine")
            {
                this.NewLine();
            }
            else
            {
                if (this.LogFile != null)
                    this.LogFile.WriteLine("[!] {0}", (object)cmd.Format.RuName);
                this.ErrorFlag = true;
            }
        }

        public void Reset()
        {
            this.Font = PrinterFonts.Pike;
            this.CurrentFontMaxChars = 80;
            this.UnsetAccentBold((EscpCommand)null);
            this.IsNormalCharSet = true;
        }

        public void Reset(EscpCommand cmd)
        {
            this.Reset();
        }

        public void Clear()
        {
            if (this.Output == null)
                return;
            this.Output.FillRectangle((Brush)new SolidBrush(Color.White), new Rectangle(0, 0, this.Paper.Width, this.Paper.Height));
        }

        public void NewPaper()
        {
            this.Clear();
            this.Reset();
            this.Margins = new Margin()
            {
                Bottom = 4,
                Left = 4,
                Right = 4,
                Top = 4
            };
            this.Cursor = new Cursor()
            {
                Line = 1,
                Column = 0,
                X = 0.0f,
                Y = (float)this.Margins.Top
            };
            this.PxPerLine = 12;
        }

        public void NewLine()
        {
            ++this.Cursor.Line;
            this.Cursor.Column = 0;
            this.HorizontalOffset = 0;
            this.Cursor.Y += (float)this.PxPerLine;
        }

        public void NewLine(EscpCommand cmd)
        {
            this.NewLine();
        }

        public void NewLineN216(EscpCommand cmd)
        {
            this.NewLine();
        }

        public void SetLeftMargin(EscpCommand cmd)
        {
            Cursor.Column = cmd.ParamBytes[0];
        }

        public void SetPike(EscpCommand cmd)
        {
            this.Font = PrinterFonts.Pike;
            this.CurrentFontMaxChars = 80;
        }

        public void SetElite(EscpCommand cmd)
        {
            this.Font = PrinterFonts.Elite;
            this.CurrentFontMaxChars = 96;
        }

        public void SetCondensed(EscpCommand cmd)
        {
            if (this.Font == PrinterFonts.Elite)
            {
                this.Font = PrinterFonts.EliteCondensed;
                this.CurrentFontMaxChars = 160;
            }
            else
            {
                if (this.Font != PrinterFonts.Pike)
                    return;
                this.Font = PrinterFonts.PikeCondensed;
                this.CurrentFontMaxChars = 137;
            }
        }

        public void SetAccentBold(EscpCommand cmd)
        {
            this.AccentBold = true;
        }

        public void UnsetAccentBold(EscpCommand cmd)
        {
            this.AccentBold = false;
        }

        public void SetDoubleStrike(EscpCommand cmd)
        {
            this.DoubleStrike = true;
        }

        public void UnsetDoubleStrike(EscpCommand cmd)
        {
            this.DoubleStrike = false;
        }

        public void ToggleDoubleWidth(EscpCommand cmd)
        {
            this.DoubleWidth = (int)cmd.ParamBytes[0] == 1;
        }

        public void ToggleDoubleHeight(EscpCommand cmd)
        {
            this.DoubleHeight = (int)cmd.ParamBytes[0] == 1;
        }

        public void ToggleUnderline(EscpCommand cmd)
        {
            this.Underline = !this.Underline;
        }

        public void SetScript(EscpCommand cmd)
        {
            if ((int)cmd.ParamBytes[0] == 1)
            {
                this.TextPosition = TextPosition.Subscript;
            }
            else
            {
                if ((int)cmd.ParamBytes[0] != 0)
                    return;
                this.TextPosition = TextPosition.Superscript;
            }
        }

        public void ResetScript(EscpCommand cmd)
        {
            this.TextPosition = TextPosition.Normal;
        }

        public void ToggleQuality(EscpCommand cmd)
        {
            if ((int)cmd.ParamBytes[0] == 0)
            {
                this.Quality = PrintQuality.Draft;
                this.Font = PrinterFonts.Pike;
            }
            else
            {
                if ((int)cmd.ParamBytes[0] != 1)
                    return;
                this.Quality = PrintQuality.LQ;
                this.Font = PrinterFonts.LetterQuality;
            }
        }

        public void SetPxPerLineN72(EscpCommand cmd)
        {
            this.PxPerLine = (int)cmd.ParamBytes[0];
        }

        public void SetPxPerLineN216(EscpCommand cmd)
        {
            this.PxPerLine = (int)cmd.ParamBytes[0] / 3;
        }

        public void AbsHorizontalOffset(EscpCommand cmd)
        {
            float num1 = (float)(256 * (int)cmd.ParamBytes[1] + (int)cmd.ParamBytes[0]) * this.Unit;
            float num2 = (float)(this.Paper.Width - 2 * this.Margins.Left) / (float)this.CurrentFontMaxChars;
            this.Cursor.Column = (int)((double)num1 / (double)num2);
            this.HorizontalOffset = (int)((double)num1 / (double)num2 - (double)this.Cursor.Column);
        }

        public void RelHorizontalOffset(EscpCommand cmd)
        {
            float num1 = (float)(256 * (int)cmd.ParamBytes[1] + (int)cmd.ParamBytes[0]) * this.Unit;
            float num2 = (float)(this.Paper.Width - 2 * this.Margins.Left) / (float)this.CurrentFontMaxChars;
            this.Cursor.Column += (int)((double)num1 / (double)num2);
            this.HorizontalOffset = (int)((double)num1 / (double)num2 - (double)(int)((double)num1 / (double)num2));
        }

        public void DefineUserSymbol(EscpCommand cmd)
        {
            if (this.UserSymbols.ContainsKey(cmd.ParamBytes[1]))
                return;
            UserSymbol userSymbol = new UserSymbol()
            {
                Bytes = cmd.SymbolBytes
            };
            this.UserSymbols.Add(cmd.ParamBytes[1], userSymbol);
        }

        public void SelectCharacterSet(EscpCommand cmd)
        {
            IsNormalCharSet = cmd.ParamBytes[0] == 0;
        }

        public void PrintText(string text)
        {
            if (!IsNormalCharSet)
            {
                var normalText = "";
                for (int i = 0, length = text.Length; i < length; i++)
                {
                    if (UserSymbols.ContainsKey((byte)text[i]))
                    {
                        if ("" != normalText)
                        {
                            PrintText(normalText);
                            normalText = "";
                        }
                        PrintSymbol(UserSymbols[(byte)text[i]]);
                    }
                    else
                    {
                        normalText += text[i];
                    }
                }
                if ("" == normalText)
                {
                    return;
                }
                text = normalText;
            }
            float num = (float)(this.Margins.Left + this.HorizontalOffset + (this.Paper.Width - this.Margins.Left - this.Margins.Right) / this.CurrentFontMaxChars * this.Cursor.Column + 1);
            float y = this.Cursor.Y;
            if (text != "\r\n" && this.LogFile != null)
            {
                this.LogFile.WriteLine();
                this.LogFile.WriteLine("Текст: '{0}'", (object)text);
                this.LogFile.WriteLine("{");
                this.LogFile.WriteLine("    Двойная ширина:{0}", DoubleWidth);
                this.LogFile.WriteLine("    Акцентирование:{0}", AccentBold);
                this.LogFile.WriteLine("    Подчеркивание:{0}", Underline);
                this.LogFile.WriteLine("    Курсор Строка:{2} Столбец:{3}, ({0},{1})", (object)num, (object)y, (object)this.Cursor.Line, (object)this.Cursor.Column);
                this.LogFile.WriteLine("}");
                this.LogFile.WriteLine();
            }
            if (text == "\r\n")
                this.NewLine();
            else if (this.DoubleWidth)
            {
                SizeF sizeF = this.Output.MeasureString(text, this.Font);
                Bitmap bitmap1 = new Bitmap((int)sizeF.Width, (int)sizeF.Height);
                using (Graphics graphics = Graphics.FromImage((Image)bitmap1))
                    graphics.DrawString(text, this.Font, (Brush)new SolidBrush(Color.Black), 0.0f, 0.0f);
                Bitmap bitmap2 = new Bitmap(bitmap1.Width * 2, bitmap1.Height);
                using (Graphics graphics = Graphics.FromImage((Image)bitmap2))
                    graphics.DrawImage((Image)bitmap1, 0, 0, bitmap1.Width * 2, bitmap1.Height);
                this.Output.DrawImage((Image)bitmap2, num, y);
                this.Cursor.Column += text.Length * 2;
            }
            else if (this.DoubleHeight)
            {
                SizeF sizeF = this.Output.MeasureString(text, this.Font);
                Bitmap bitmap1 = new Bitmap((int)sizeF.Width, (int)sizeF.Height);
                using (Graphics graphics = Graphics.FromImage((Image)bitmap1))
                    graphics.DrawString(text, this.Font, (Brush)new SolidBrush(Color.Black), 0.0f, 0.0f);
                Bitmap bitmap2 = new Bitmap(bitmap1.Width, bitmap1.Height * 2);
                using (Graphics graphics = Graphics.FromImage((Image)bitmap2))
                    graphics.DrawImage((Image)bitmap1, 0, 0, bitmap1.Width, bitmap1.Height * 2);
                this.Output.DrawImage((Image)bitmap2, num, y);
                this.Cursor.Column += text.Length;
            }
            else if (this.TextPosition == TextPosition.Subscript)
            {
                this.Output.DrawString(text, new Font("Courier", this.Font.Size * 0.75f), (Brush)new SolidBrush(Color.Black), num + (float)((double)(this.Paper.Width - this.Margins.Left - this.Margins.Right) / (double)this.CurrentFontMaxChars * 0.25), y + (float)this.PxPerLine * 0.667f);
                this.Cursor.Column += text.Length;
            }
            else
            {
                if (this.AccentBold)
                    this.Output.DrawString(text, this.Font, (Brush)new SolidBrush(Color.FromArgb(192, 0, 0, 0)), num + 1f, y);
                if (this.DoubleStrike)
                    this.Output.DrawString(text, this.Font, (Brush)new SolidBrush(Color.FromArgb(192, 0, 0, 0)), num, y - 1f);
                if (this.Underline)
                    this.Output.DrawLine(new Pen(Color.Black), num, y + 14f, num + this.Output.MeasureString(text, this.Font).Width, y + 14f);
                this.Output.DrawString(text, this.Font, (Brush)new SolidBrush(Color.Black), num, y);
                this.Cursor.Column += text.Length;
            }
        }

        public void PrintSymbol(UserSymbol symbol)
        {
            printGraphicsFromBytes(symbol.Bytes);
        }

        public void SetGraphicsMode(EscpCommand cmd)
        {
        }

        public void PrintGraphics(EscpCommand cmd)
        {
            printGraphicsFromBytes(cmd.SymbolBytes);
        }

        private void printGraphicsFromBytes(byte[] bytes, int digits = 8)
        {
            float columnWidth = (Paper.Width - Margins.Left - Margins.Right) / 80;
            float startX = (Margins.Left + HorizontalOffset + columnWidth * Cursor.Column + 1);
            float startY = Cursor.Y;
            var length = bytes.Length;
            int x, y;
            Pen pen;

            var strings = new string[digits];
            for (int i = 0; i < digits; i++)
            {
                strings[i] = "";
            }

            for (int currentDx = 0; currentDx < length; ++currentDx)
            {
                x = (int)startX + currentDx;
                for (int pixel = 0, digitMask = 1; pixel < digits; ++pixel, digitMask <<= 1)
                {
                    y = (int)startY - pixel;
                    pen = (bytes[currentDx] & digitMask) == 0 ? whitePen : blackPen;
                    Output.DrawRectangle(pen, x, y, 1, 1);
                    strings[pixel] = (pen == whitePen ? ' ' : '#') + strings[pixel];
                }
            }

            var output = "";
            for (int i = digits - 1; i >= 0; i--)
            {
                output += strings[i] + "\n";
            }
            if (null != LogFile)
            {
                LogFile.WriteLine("Print graphical object: ");
                LogFile.Write(output);
            }

            Cursor.Column += length / columnWidth;
        }
    }
}
