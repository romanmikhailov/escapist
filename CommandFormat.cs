﻿namespace Escapist
{
    public struct CommandFormat
    {
        public byte CmdByte;
        public int Length;
        public string Name;
        public string RuName;
    }
}