﻿namespace Escapist
{
    public class Paper
    {
        public int Width { get; private set; }

        public int Height { get; private set; }

        public float Resolution { get; set; }

        public Paper(int pixelWidth, int pixelHeight)
        {
            this.Width = pixelWidth;
            this.Height = pixelHeight;
        }
    }
}