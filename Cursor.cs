﻿namespace Escapist
{
    public struct Cursor
    {
        public int Line;
        public float Column;
        public float X;
        public float Y;
    }
}