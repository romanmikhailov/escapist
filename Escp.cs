﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Escapist
{
    public static class Escp
    {
        public static readonly CommandFormat UnknownCommand = new CommandFormat()
        {
            CmdByte = 0,
            Name = "UnknownCmd",
            RuName = "Неизвестная или неправильно сформированная команда"
        };
        public static Dictionary<string, CommandFormat> Commands = new Dictionary<string, CommandFormat>();
        public static CommandFormat NewLineCommand = new CommandFormat()
        {
            CmdByte = 13,
            Length = 1,
            Name = "NewLine"
        };
        public const int Esc = 27;
        public const int NewLine = 13;

        static Escp()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("fx800.xml");
            foreach (XmlNode xmlNode in xmlDocument.SelectNodes("/Printer/Commands/Command"))
                Escp.Commands.Add(xmlNode.Attributes["name"].Value, new CommandFormat()
                {
                    CmdByte = Convert.ToByte(xmlNode.Attributes["cmdbyte"].Value, 16),
                    Length = int.Parse(xmlNode.Attributes["length"].Value),
                    Name = xmlNode.Attributes["name"].Value,
                    RuName = xmlNode.Attributes["runame"].Value
                });
        }

        public static CommandFormat GetFormat(byte cmdByte)
        {
            IEnumerable<CommandFormat> source = Enumerable.Select<KeyValuePair<string, CommandFormat>, CommandFormat>(Enumerable.Where<KeyValuePair<string, CommandFormat>>((IEnumerable<KeyValuePair<string, CommandFormat>>)Escp.Commands, (Func<KeyValuePair<string, CommandFormat>, bool>)(c => (int)c.Value.CmdByte == (int)cmdByte)), (Func<KeyValuePair<string, CommandFormat>, CommandFormat>)(c => c.Value));
            if (Enumerable.Count<CommandFormat>(source) == 0)
                return Escp.UnknownCommand;
            return Enumerable.First<CommandFormat>(source);
        }
    }
}
