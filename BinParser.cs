﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Escapist
{
    internal class BinParser
    {
        private List<int> cmdIndexes = new List<int>();
        public List<byte[]> CmdList = new List<byte[]>();
        private byte[] binData;

        public int CurrentCmdIndex { get; private set; }

        public void ReadFile(string fileName)
        {
            using (BinaryReader binaryReader = new BinaryReader((Stream)File.Open(fileName, FileMode.Open)))
            {
                int count = (int)binaryReader.BaseStream.Length;
                this.binData = binaryReader.ReadBytes(count);
                for (int index = 0; index < count; ++index)
                {
                    if ((int)this.binData[index] == 27)
                        this.cmdIndexes.Add(index);
                    if ((int)this.binData[index] == 13 && index + 1 < count && (int)this.binData[index + 1] == 10)
                        this.cmdIndexes.Add(index);
                }
                this.cmdIndexes.Add(count);
            }
            this.Parse();
        }

        private void Parse()
        {
            for (; this.CurrentCmdIndex < this.cmdIndexes.Count - 1; ++this.CurrentCmdIndex)
            {
                int length = this.cmdIndexes[this.CurrentCmdIndex + 1] - this.cmdIndexes[this.CurrentCmdIndex];
                byte[] numArray = new byte[length];
                Array.Copy((Array)this.binData, this.cmdIndexes[this.CurrentCmdIndex], (Array)numArray, 0, length);
                this.CmdList.Add(numArray);
            }
        }
    }
}
