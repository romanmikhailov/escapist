﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Escapist
{
    public class OffsetCalcForm : Form
    {
        private IContainer components;
        private Button closeButton;
        private Label label1;
        private TextBox cmTextBox;
        private Label resultLabel;
        private Label n216Label;
        private Label n72Label;

        public OffsetCalcForm()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.closeButton = new Button();
            this.label1 = new Label();
            this.cmTextBox = new TextBox();
            this.resultLabel = new Label();
            this.n216Label = new Label();
            this.n72Label = new Label();
            this.SuspendLayout();
            this.closeButton.Location = new Point(110, 167);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new Size(75, 23);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Закрыть";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new EventHandler(this.closeButton_Click);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(119, 41);
            this.label1.Name = "label1";
            this.label1.Size = new Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "см";
            this.cmTextBox.Location = new Point(12, 38);
            this.cmTextBox.Name = "cmTextBox";
            this.cmTextBox.Size = new Size(101, 20);
            this.cmTextBox.TabIndex = 2;
            this.cmTextBox.TextChanged += new EventHandler(this.textBox1_TextChanged);
            this.resultLabel.AutoSize = true;
            this.resultLabel.Location = new Point(12, 74);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new Size(0, 13);
            this.resultLabel.TabIndex = 3;
            this.n216Label.AutoSize = true;
            this.n216Label.Location = new Point(107, 96);
            this.n216Label.Name = "n216Label";
            this.n216Label.Size = new Size(0, 13);
            this.n216Label.TabIndex = 4;
            this.n72Label.AutoSize = true;
            this.n72Label.Location = new Point(107, 74);
            this.n72Label.Name = "n72Label";
            this.n72Label.Size = new Size(0, 13);
            this.n72Label.TabIndex = 5;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(293, 202);
            this.Controls.Add((Control)this.n72Label);
            this.Controls.Add((Control)this.n216Label);
            this.Controls.Add((Control)this.resultLabel);
            this.Controls.Add((Control)this.cmTextBox);
            this.Controls.Add((Control)this.label1);
            this.Controls.Add((Control)this.closeButton);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OffsetCalcForm";
            this.Text = "Калькулятор отступов";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.cmTextBox.Text = this.cmTextBox.Text.Replace('.', ',');
            this.cmTextBox.SelectionStart = this.cmTextBox.Text.Length;
            this.cmTextBox.ScrollToCaret();
            try
            {
                double num = (string.IsNullOrEmpty(this.cmTextBox.Text) ? 0.0 : Convert.ToDouble(this.cmTextBox.Text)) * 0.393700787;
                this.resultLabel.Text = (string)(object)num + (object)"\"";
                this.n72Label.Text = (string)(object)Math.Round(num * 72.0) + (object)"/72";
                this.n216Label.Text = (string)(object)Math.Round(num * 216.0) + (object)"/216";
            }
            catch
            {
                int num = (int)MessageBox.Show("Ошибка преобразования", "Escapist", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.cmTextBox.Text = "";
            }
        }
    }
}
